# set cmake minimum version
cmake_minimum_required(VERSION 3.10)

# set the project name and version
project(Education_system VERSION 1.0)

# include and link source folder
include_directories(src)
link_directories(src)

# add the executable
add_executable(main src/main.cpp)

# add add library
add_library(Education_system_Lib src/education_system.cpp)
add_library(Control_Lib src/view_control.cpp)
add_library(Course_Lib src/course.cpp)
add_library(Teacher_Lib src/teacher.cpp)
add_library(Student_Lib src/student.cpp)
add_library(Department_Lib src/department.cpp)
add_library(Program_Lib src/program.cpp)
add_library(Entity_Lib src/entity.cpp)
add_library(Connection_Lib src/connection.cpp)

# link the library to the executable
target_link_libraries(main Education_system_Lib)
target_link_libraries(main Control_Lib)
target_link_libraries(main Course_Lib)
target_link_libraries(main Teacher_Lib)
target_link_libraries(main Student_Lib)
target_link_libraries(main Department_Lib)
target_link_libraries(main Program_Lib)
target_link_libraries(main Entity_Lib)
target_link_libraries(main Connection_Lib)

# specify the C++ standard
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED True)

# make release the default build
if(NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE Release)
  message(STATUS "Build type not specified: Use Release by default")
endif()

set(CMAKE_CXX_FLAGS "-Wall -Wextra")
set(CMAKE_CXX_FLAGS_DEBUG "-g")
set(CMAKE_CXX_FLAGS_RELEASE "-O")

#set(CMAKE_CXX_FLAGS "-std=c++17 -Wall -Wextra")