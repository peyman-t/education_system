#include "program.h"
#include <iostream>
#include <vector>

Program::Program (std::string name, float acceptance_rate, int id)
{
    this->name = name;
    this->acceptance_rate = acceptance_rate;
    this->id =id;
}

void Program::Print()
{
    std::cout << this->id << ": ";
    std::cout << this->name <<", ";
    std::cout << "acceptance: " << this->acceptance_rate << std::endl;
}