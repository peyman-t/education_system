#ifndef DEPARTMENT_H
#define DEPARTMENT_H
#include <string>
#include "entity.h"
class Department : public Entity 
{
    private:
        int id;
        std::string code;
    public:
        std::string name;
        Department(){};
        Department (int id, std::string name, std::string code);
        void Print();
        int Get_id() { return id; }
        std::string Get_code() {return code; }
        template <class B>
        void serialize(B& buf) const 
        {
            buf << id << code << name;
        }
        template <class B>
        void parse(B& buf) 
        {
            buf >> id >> code >> name;
        }
};
#endif