#include "department.h"
#include <string>
#include <iostream>
#include <vector>

Department::Department (int id, std::string name, std::string code)
{
    this->id=id;
    this->code = code;
    this->name = name;
}

void Department::Print()
{
    std::cout << this->id << ": ";
    std::cout << this->code << " ";
    std::cout << this->name << std::endl;
}
