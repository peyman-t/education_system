#include "student.h"
#include <vector>
Student::Student (std::string f_name, std::string s_name, int id) : Person (f_name, s_name) 
{
    this->id = id;
}
void Student::Print()
{
    std::cout << this->id << ": ";
    std::cout << this->f_name << ' ' << this->s_name << std::endl;        
}