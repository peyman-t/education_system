#ifndef CONNECTION_H
#define CONNECTION_H
#include "entity.h"
#include <vector>
#include <string>
/*
*@brief The class represents a connection between two entities (e_1 and e_2).
*/
class Connection
{
    Entity * e_1;
    Entity * e_2;
    public: 
        Connection (Entity * e1, Entity* e2);
        Entity * Get_e1() { return this->e_1; }
        Entity * Get_e2() { return this->e_2; }
        static bool Remove_from_list_by_e2(Entity * e2, std::vector<Connection *> &connections);
        static bool Remove_from_list_by_e1(Entity * e1, std::vector<Connection *> &connections);
        static bool Remove_from_list_by_e1_and_e2(Entity * e1, Entity *e2, std::vector<Connection *> &connections);
        static bool Add_to_list(Entity * e1, Entity * e2, std::vector<Connection *> &connections);
        static bool Print_from_list_by_e1 (Entity * e1, std::vector<Connection *> &connections);
        static bool Print_from_list_by_e2 (Entity * e2, std::vector<Connection *> &connections);
        template <class B>
        void serialize(B& buf) const    
        {
            std::pair<int,int> p = std::make_pair(e_1->Get_id() , e_2->Get_id());
            buf << p;
        }
};
#endif