#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include "teacher.h"
#include "student.h"
#include "course.h"
#include "education_system.h"
#include "../hps/src/hps.h"

Education_System::Education_System()
{            
    Read_entities_from_file<Student>("../db_hps/students.txt", students);
    Read_entities_from_file<Teacher>("../db_hps/teachers.txt", teachers); 
    Read_entities_from_file<Course>("../db_hps/courses.txt", courses);
    Read_entities_from_file<Department>("../db_hps/departments.txt", departments); 
    Read_entities_from_file<Program>("../db_hps/programs.txt", programs);
    Read_connections_from_file("../db_hps/student_course.txt", student_course, students, courses);
    Read_connections_from_file("../db_hps/teacher_course.txt",teacher_course, teachers,courses);
    Read_connections_from_file("../db_hps/teacher_department.txt",teacher_department, teachers,departments);
    Read_latest_id_from_file("../db_hps/latest_id.txt");                
}
Education_System::~Education_System()
{
    for ( Entity *t : teachers) { delete t; }            
    for ( Entity *s : students) { delete s; }
    for ( Entity *c : courses) { delete c; }
    for ( Entity *d : departments) { delete d; }
    for ( Entity *p : programs) { delete p; }
    for ( Connection *c : teacher_course) { delete c; }
    for ( Connection *c : teacher_department) { delete c; }
    for ( Connection *c : student_course) { delete c; }            
}
template <class A>
void Education_System::Save_entities_to_file(std::string file_name, std::vector<Entity *> &v) 
{
    std::vector<A> entity_v;
    for (auto it = v.begin(); it != v.end(); ++it) 
    {
        A * a = dynamic_cast<A*>(*it);        
        entity_v.push_back(*a);
    }
    std::ofstream dataFile;  
    std::string serialized = hps::to_string(entity_v);
    dataFile.open(file_name, std::ios::binary);
    if(!dataFile.is_open())
        std::cout <<"File not found";
    dataFile << serialized;
    dataFile.close();  
}
template void Education_System::Save_entities_to_file<Student>(std::string file_name, std::vector<Entity *> &v);
template void Education_System::Save_entities_to_file<Teacher>(std::string file_name, std::vector<Entity *> &v);
template void Education_System::Save_entities_to_file<Course>(std::string file_name, std::vector<Entity *> &v);
template void Education_System::Save_entities_to_file<Department>(std::string file_name, std::vector<Entity *> &v);
template void Education_System::Save_entities_to_file<Program>(std::string file_name, std::vector<Entity *> &v);

template <class A>
void Education_System::Read_entities_from_file(std::string file_name, std::vector<Entity*> &v) 
{
    std::ifstream dataFile;            
    dataFile.open(file_name, std::ios::binary);
    if(!dataFile.is_open())
        std::cout<< "File " << file_name <<" is not found"<<std::endl;
    auto parsed = hps::from_stream<std::vector<A>>(dataFile);
    for (auto it = parsed.begin(); it != parsed.end(); ++it) 
    {
        A * a = new A(*it);
        v.push_back(a);
    }
}
template void Education_System::Read_entities_from_file<Student>(std::string file_name, std::vector<Entity *> &v);
template void Education_System::Read_entities_from_file<Teacher>(std::string file_name, std::vector<Entity *> &v);
template void Education_System::Read_entities_from_file<Course>(std::string file_name, std::vector<Entity *> &v);
template void Education_System::Read_entities_from_file<Department>(std::string file_name, std::vector<Entity *> &v);
template void Education_System::Read_entities_from_file<Program>(std::string file_name, std::vector<Entity *> &v);

void Education_System::Save_connections_to_file(std::string file_name, std::vector<Connection *> & v)
{
    std::vector<Connection> connect_v;
    for (auto it = v.begin(); it != v.end(); ++it) 
    {       
        connect_v.push_back(**it);
    }
    std::ofstream dataFile;  
    std::string serialized = hps::to_string(connect_v);
    dataFile.open(file_name, std::ios::binary);
    if(!dataFile.is_open())
        std::cout<< "File " << file_name <<" is not found"<<std::endl;
    dataFile << serialized;
    dataFile.close();   
}

void Education_System::Read_connections_from_file(std::string file_name, std::vector<Connection *> &v, std::vector<Entity *> &v1 , std::vector<Entity *> &v2) 
{
    std::ifstream dataFile;            
    dataFile.open(file_name, std::ios::binary);
    if(!dataFile.is_open())
        std::cout<< "File " << file_name <<" is not found"<<std::endl;
    auto parsed = hps::from_stream<std::vector<std::pair<int,int>>>(dataFile);
    for (auto it = parsed.begin(); it != parsed.end(); ++it) 
    {        
        Entity * e1 = Entity::Get_from_list_by_id(it->first,v1);
        Entity * e2 = Entity::Get_from_list_by_id(it->second,v2);
        Connection * c = new Connection(e1, e2);
        v.push_back(c);        
    }                                         
}

void  Education_System::Save_latest_id_to_file (std::string file_name)
{
    std::vector<int> ids ={ last_student_id,last_teacher_id,last_course_id,last_program_id,last_department_id };
    std::string serialized =hps::to_string(ids);
    std::ofstream dataFile;
    dataFile.open(file_name, std::ios::binary);
    if(!dataFile.is_open())
        std::cout<< "File " << file_name <<" is not found"<<std::endl;
    dataFile << serialized;
    dataFile.close(); 
}

void  Education_System::Read_latest_id_from_file(std::string file_name) 
{
    std::ifstream dataFile;            
    dataFile.open(file_name, std::ios::binary);
    if(!dataFile.is_open())
        std::cout<< "File " << file_name <<" is not found"<<std::endl;     
    auto parsed = hps::from_stream<std::vector<int>>(dataFile);
    dataFile.close();      
    if (parsed.empty())
        return;
    this->last_student_id = parsed[0];
    this->last_teacher_id = parsed[1];
    this->last_course_id = parsed[2];
    this->last_program_id = parsed[3];
    this->last_department_id = parsed[4];            
}

void  Education_System::Save ()
{
    Save_entities_to_file<Student>("../db_hps/students.txt", students);
    Save_entities_to_file<Teacher>("../db_hps/teachers.txt", teachers);
    Save_entities_to_file<Course>("../db_hps/courses.txt", courses);
    Save_entities_to_file<Department>("../db_hps/departments.txt", departments);
    Save_entities_to_file<Program>("../db_hps/programs.txt", programs);
    Save_connections_to_file("../db_hps/student_course.txt",student_course);
    Save_connections_to_file("../db_hps/teacher_course.txt",teacher_course);
    Save_connections_to_file("../db_hps/teacher_department.txt", teacher_department);
    Save_latest_id_to_file("../db_hps/latest_id.txt");
}

bool  Education_System::Add_teacher(std::string f_name, std::string s_name, float salary)
{
    Teacher * teacher = new Teacher(f_name, s_name, salary, last_teacher_id++);
    if (teacher==nullptr) return false;
    teachers.push_back(teacher);           
    return true; 
}
bool  Education_System::Add_student(std::string f_name, std::string s_name)
{
    Student * student = new Student(f_name, s_name, last_student_id++);
    if (student == nullptr) return false;
    students.push_back(student);
    return true;
}
bool Education_System::Add_course(std::string name, int points)
{
    Course * course = new Course(name, points, last_course_id++);
    if (course == nullptr) return false;
    courses.push_back(course);
    return true;
}
bool Education_System::Add_program(std::string name, float acceptance_rate)
{
    Program * program = new Program(name, acceptance_rate, last_program_id++);
    if (program == nullptr) return false;
    programs.push_back(program);
    return true;
}
bool Education_System::Add_department(std::string name)
{
    int id = last_department_id++;
    std::string code = std::to_string(id);
    code.append(std::to_string(std::toupper(name[0])));
    code.append(std::to_string(std::toupper(name[1])));
    Department * department = new Department(id, name, code );
    if (department == nullptr) return false;
    departments.push_back(department);
    return true;
}
bool  Education_System::Remove_teacher(int id)
{
    Entity * t = Entity::Remove_from_list_by_id(id, teachers);
    if (t == nullptr) return false;
    Connection::Remove_from_list_by_e1(t, teacher_course);
    Connection::Remove_from_list_by_e1(t, teacher_department);
    delete t;
    return true;
}

bool  Education_System::Remove_student(int id)
{
    Entity * s = Entity::Remove_from_list_by_id(id, students);
    if (s==nullptr) return false;
    Connection::Remove_from_list_by_e1(s, student_course);
    delete s;
    return true;
}

bool  Education_System::Remove_course(int id)
{
    Entity * c = Entity::Remove_from_list_by_id(id, courses);
    if (c==nullptr) return false;
    Connection::Remove_from_list_by_e2(c, teacher_course);
    Connection::Remove_from_list_by_e2(c, student_course);   
    delete c;
    return true;
}   

bool  Education_System::Remove_program(int id)
{
    Entity * p = Entity::Remove_from_list_by_id(id, programs);
    if (p==nullptr) return false;
    delete p;
    return false;    
}   

bool  Education_System::Remove_department(int id)
{
    Entity * d = Entity::Remove_from_list_by_id(id, departments);
    if (d==nullptr) return false;
    Connection::Remove_from_list_by_e2(d, teacher_department);
    delete d;
    return true;
}   

bool Education_System::Assign_teacher_to_department (int teacher_id, int department_id)
{
    Entity * t = Entity::Get_from_list_by_id(teacher_id, teachers);
    Entity * d = Entity::Get_from_list_by_id(department_id, departments);
    if (t ==nullptr || d == nullptr) return false;
    return Connection::Add_to_list( t,d, teacher_department);
}     

bool Education_System::Assign_teacher_to_course (int teacher_id, int course_id)
{
    Entity * t = Entity::Get_from_list_by_id(teacher_id, teachers);
    Entity * c = Entity::Get_from_list_by_id(course_id, courses);
    if (t ==nullptr || c == nullptr) return false;
    return Connection::Add_to_list(t, c, teacher_course);
}   

bool Education_System::Assign_student_to_course (int student_id, int course_id)
{
    Entity * s = Entity::Get_from_list_by_id(student_id, students);
    Entity * c = Entity::Get_from_list_by_id(course_id, courses);
    if (s ==nullptr || c == nullptr) return false;
    return Connection::Add_to_list(s, c, student_course); 
}

bool Education_System::Remove_teacher_from_department (int teacher_id, int department_id)
{
    Entity * t = Entity::Get_from_list_by_id(teacher_id, teachers);
    Entity * d = Entity::Get_from_list_by_id(department_id, departments);
    if (t ==nullptr || d == nullptr) return false;
    return Connection::Remove_from_list_by_e1_and_e2(t,d,teacher_department);
}

bool Education_System::Remove_teacher_from_course (int teacher_id, int course_id)
{
    Entity * t = Entity::Get_from_list_by_id(teacher_id, teachers);
    Entity * c = Entity::Get_from_list_by_id(course_id, courses);
    if (t ==nullptr || c == nullptr) return false;
    return Connection::Remove_from_list_by_e1_and_e2(t,c,teacher_course);
}

bool  Education_System::Remove_student_from_course (int student_id, int course_id)
{
    Entity * s = Entity::Get_from_list_by_id(student_id, students);
    Entity * c = Entity::Get_from_list_by_id(course_id, courses);
    if (s ==nullptr || c == nullptr) return false;
    return Connection::Remove_from_list_by_e1_and_e2(s,c,student_course);
}

void Education_System::Print_teacher_courses (int teacher_id)
{
    Entity * t = Entity::Get_from_list_by_id(teacher_id, teachers);
    if (!Connection::Print_from_list_by_e1(t, teacher_course))
        std::cout << "Teacher is not assigned to any course"<<std::endl;
}

void Education_System::Print_student_courses (int student_id)
{
    Entity * s = Entity::Get_from_list_by_id(student_id, students);
    if (!Connection::Print_from_list_by_e1(s, student_course))
        std::cout << "Student is not assigned to any course"<<std::endl;
}

void Education_System::Print_course_students (int course_id)
{
    Entity * c = Entity::Get_from_list_by_id(course_id, courses);
    if (!Connection::Print_from_list_by_e2(c, student_course))
        std::cout << "No students assigned to this course"<<std::endl;
}

void Education_System::Print_course_teachers (int course_id)
{
    Entity * c = Entity::Get_from_list_by_id(course_id, courses);
    if (!Connection::Print_from_list_by_e2(c, teacher_course))
        std::cout << "No teachers assigned to this course"<<std::endl;
}

void Education_System::Print_teacher_departments (int teacher_id)
{
    Entity * t = Entity::Get_from_list_by_id(teacher_id, teachers);
    if (!Connection::Print_from_list_by_e1(t, teacher_department))
        std::cout << "Teacher is not assigned to any department"<<std::endl;
}

void Education_System::Print_department_teachers (int department_id)
{
    Entity * d = Entity::Get_from_list_by_id(department_id, departments);
    if (!Connection::Print_from_list_by_e2(d, teacher_department))
        std::cout << "No teachers assigned to this department"<<std::endl;
}

void  Education_System::Print_system()
{
    if (!departments.empty())
    {
        std::cout <<std::endl<< "Departments: "<<std::endl;
        Entity::Print_list(departments);
    }
    if (!teachers.empty())
    {
        std::cout <<std::endl<< "Teachers: "<< std::endl;
        Entity::Print_list(teachers);
    }
    if (!students.empty())
    {
        std::cout <<std::endl<< "Students: "<< std::endl;
        Entity::Print_list(students);
    }
    if (!courses.empty())
    {
        std::cout <<std::endl<< "Courses: "<<std::endl;
        Entity::Print_list(courses);
    }        
    if (!programs.empty())
    {
        std::cout <<std::endl<< "Programs: "<<std::endl;
        Entity::Print_list(programs);
    }
}

