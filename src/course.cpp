#include "course.h"
#include <iostream>
#include <vector>

Course::Course (std::string course_name, float points, int id)
{
    this->course_name = course_name;
    this->points = points;
    this->id =id;
}
void Course::Print()
{
    std::cout << this->id << ": ";
    std::cout << this->course_name << ", ";
    std::cout << this->points << " credits" << std::endl;
}