#ifndef STUDENT_H
#define STUDENT_H
#include "person.h"
#include "entity.h"
#include <iostream>
#include <string>

class Student : public Person , public Entity
{
    private:     
        int id;        
    public:
        Student(){};
        Student (std::string f_name, std::string s_name, int id);
        void Print();        
        int Get_id() { return id; }
        template <class B>
        void serialize(B& buf) const 
        {
            buf << id << f_name << s_name;
        }
        template <class B>
        void parse(B& buf) 
        {
            buf >> id >> f_name >> s_name;
        }
};
#endif