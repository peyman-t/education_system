#ifndef COURSE_H
#define COURSE_H
#include "entity.h"
#include <string>

class Course : public Entity
{
    private:
        int id;
        float points;
        std::string course_name;
    public:
        Course(){};
        Course (std::string course_name, float points, int id);
        void Print();
        int Get_id() { return id; }
        template <class B>
        void serialize(B& buf) const 
        {
            buf << id << course_name << points;
        }
        template <class B>
        void parse(B& buf) 
        {
            buf >> id >> course_name >> points;
        }
};
#endif