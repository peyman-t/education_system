#ifndef ENTITY_H
#define ENTITY_H
#include <string>
#include <vector>
class Entity
{
    public:        
        virtual void Print()=0;
        virtual int Get_id()=0;
        virtual ~Entity(){};
        static Entity *  Get_from_list_by_id(int id, std::vector<Entity*> &v);
        static Entity * Remove_from_list_by_id(int id, std::vector<Entity *> &v);
        static void Print_from_list_by_id (int id, std::vector<Entity *> &v);
        static void Print_list( std::vector<Entity *> &v);
};
#endif