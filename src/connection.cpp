#include "connection.h"
#include "entity.h"
#include <vector>
#include <string>
#include "../hps/src/hps.h"
Connection::Connection (Entity * e1, Entity* e2)
{
    this->e_1=e1;
    this->e_2=e2;
}
bool Connection::Remove_from_list_by_e2(Entity * e2, std::vector<Connection *> &connections)
{
    bool found=false;
    for (auto itr = connections.begin(); itr != connections.end(); ) 
    {
        if ((*itr)->Get_e2() == e2)
        {
            delete *itr;
            connections.erase(itr);
            found=true;
        }            
        else 
            itr++;
    }   
    return found;
}
bool Connection::Remove_from_list_by_e1(Entity * e1, std::vector<Connection *> &connections)
{
    bool found=false;
    for (auto itr = connections.begin(); itr != connections.end(); ) 
    {
        if ((*itr)->Get_e1() == e1)
        {
            delete *itr;
            connections.erase(itr);
            found=true;
        }            
        else 
            itr++;
    }   
    return found;
}
bool Connection::Remove_from_list_by_e1_and_e2(Entity * e1, Entity *e2, std::vector<Connection *> &connections)
{
    for (auto itr = connections.begin(); itr != connections.end(); itr++ ) 
    {
        if ((*itr)->Get_e1() == e1 && (*itr)->Get_e2() == e2)
        {
            delete *itr;
            connections.erase(itr);
            return true;
        }
    }
    return false;
}
bool Connection::Add_to_list(Entity * e1, Entity * e2, std::vector<Connection *> &connections)
{
    for (auto itr = connections.begin(); itr != connections.end(); ++itr) 
    {
        if ((*itr)->Get_e1() == e1 && (*itr)->Get_e2()== e2)  //already exist
            return false;
    }
    connections.push_back(new Connection(e1,e2));
    return true;
}
bool Connection::Print_from_list_by_e1 (Entity * e1, std::vector<Connection *> &connections)
{
    bool found=false;
    for (auto itr = connections.begin(); itr != connections.end(); ++itr) 
    {                
        if ((*itr)->Get_e1() == e1)
        {
            Entity * entity = (*itr)->Get_e2();
            entity->Print();
            found=true;
        }                    
    }
    return found;
}
bool  Connection::Print_from_list_by_e2 (Entity * e2, std::vector<Connection *> &connections)
{
    bool found=false;
    for (auto itr = connections.begin(); itr != connections.end(); ++itr) 
    {                
        if ((*itr)->Get_e2() == e2)
        {
            Entity * entity = (*itr)->Get_e1();
            entity->Print();
            found=true;
        }                    
    }
    return found;
}