#include "entity.h"
#include <vector>
#include <iostream>
Entity *  Entity::Get_from_list_by_id(int id, std::vector<Entity*> &v)
{
    for (auto it = v.begin(); it != v.end(); ++it) 
    {
        if ((*it)->Get_id() == id)
        {
            return *it;
        }
    }
    return nullptr;
}
Entity * Entity::Remove_from_list_by_id(int id, std::vector<Entity *> &v) 
{
    for (auto it = v.begin(); it != v.end(); ++it) 
    {
        if ((*it)->Get_id() == id)
        {        
            Entity * e = *(it);
            v.erase(it);
            return e;
        }
    }
    return nullptr;
}
void Entity::Print_from_list_by_id (int id, std::vector<Entity *> &v)
{
    Entity * e = Entity::Get_from_list_by_id(id, v);
    if (e == nullptr)
        std::cout << "Nothing with id "<< id <<" found"<<std::endl;
    e->Print();
} 
void Entity::Print_list( std::vector<Entity *> &v)
{
    for (auto itr = v.begin(); itr != v.end(); ++itr) 
    {                
        (*itr)->Print();                  
    }
}
