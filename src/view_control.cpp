#include "view_control.h"

void View_Control::Show_delimeter()
{
    std::cout << "==================================" <<std::endl;
}
void View_Control::Show_msg(std::string s)
{
    std::cout << s;
}
void View_Control::Show_endl(std::string s)
{
    std::cout << s <<std::endl;
}

void View_Control::Display_teacher(Education_System &es)
{
    Show_msg("Enter teacher id: ");
    int id;
    std::cin >> id;
    std::cout<<std::endl;
    Entity::Print_from_list_by_id(id, es.teachers);
    std::cout<< "Assigned to courses: " << std::endl;
    es.Print_teacher_courses(id);
    std::cout<< "Assigned to department: "<<std::endl;
    es.Print_teacher_departments(id);
    std::cout<<std::endl;
}

void View_Control::Display_student(Education_System &es)
{
    Show_msg("Enter student id: ");
    int id;
    std::cin >> id;
    std::cout<<std::endl;
    Entity::Print_from_list_by_id(id, es.students);
    std::cout<< "Assigned to courses: "<< std::endl;
    es.Print_student_courses(id);
    std::cout<<std::endl;
}
void View_Control::Display_course(Education_System &es)
{
    Show_msg("Enter course id: ");
    int id;
    std::cin >> id;
    std::cout<<std::endl;
    Entity::Print_from_list_by_id(id, es.courses);
    std::cout<< "Students assigned to this course: " << std::endl;
    es.Print_course_students(id);
    std::cout<<std::endl;
    std::cout<< "Teachers assigned to this course: " << std::endl;
    es.Print_course_teachers(id);
    std::cout<<std::endl;
}
void View_Control::Display_department(Education_System &es)
{
    Show_msg("Enter department id: ");
    int id;
    std::cin >> id;
    std::cout<<std::endl;
    Entity::Print_from_list_by_id(id, es.departments);
    std::cout<< "Teachers assigned to this department: " << std::endl;
    es.Print_department_teachers(id);
    std::cout<<std::endl;
}
void View_Control::Display_program(Education_System &es)
{
    Show_msg("Enter program id: ");
    int id;
    std::cin >> id;
    std::cout<<std::endl;
    Entity::Print_from_list_by_id(id, es.programs);
    std::cout<<std::endl;
}
void View_Control::Add_teacher(Education_System &es)
{
    
    std::string f_name;
    std::string s_name;
    Show_msg("Enter first name: ");
    std::cin.ignore();
    std::getline(std::cin, f_name);
    Show_msg("Enter second name: ");
    getline(std::cin, s_name);    
    Show_msg("Enter salary: ");
    float salary;
    std::cin >> salary;
    if (es.Add_teacher(f_name, s_name, salary))
        std::cout << "Added"<<std::endl;
    std::cout<<std::endl;
}
void View_Control::Add_student(Education_System &es)
{
    std::string f_name;
    std::string s_name;
    Show_msg("Enter first name: ");
    std::cin.ignore();    
    std::getline(std::cin, f_name);
    Show_msg("Enter second name: ");    
    std::getline(std::cin, s_name);
    if (es.Add_student(f_name, s_name))
        std::cout << "Added"<<std::endl;
    std::cout<<std::endl;
}
void View_Control::Add_course(Education_System &es)
{
    Show_msg("Enter name: ");
    std::string name;
    std::cin.ignore();    
    std::getline(std::cin, name);
    Show_msg("Enter credits: ");
    float points;
    std::cin >> points;
    if (es.Add_course(name, points))
        std::cout << "Added"<<std::endl;
    std::cout<<std::endl;
}
void View_Control::Add_department(Education_System &es)
{
    Show_msg("Enter name: ");
    std::string name;
    std::cin.ignore();    
    std::getline(std::cin, name);
    if (es.Add_department(name))
        std::cout << "Added"<<std::endl;
    std::cout<<std::endl;
}
void View_Control::Add_program(Education_System &es)
{
    Show_msg("Enter name: ");
    std::string name;
    std::cin.ignore();    
    std::getline(std::cin, name);
    Show_msg("Enter acceptance rate: ");
    float rate;
    std::cin >> rate;
    if (es.Add_program(name, rate))
        std::cout << "Added"<<std::endl;
    std::cout<<std::endl;
}

void View_Control::Remove_teacher(Education_System &es)
{    
    Show_msg("Enter teacher id: ");
    int id;
    std::cin >> id;
    if (es.Remove_teacher(id))
        std::cout << "Removed"<<std::endl;
    std::cout<<std::endl;
}

void View_Control::Remove_student(Education_System &es)
{    
    Show_msg("Enter student id: ");
    int id;
    std::cin >> id;
    if (es.Remove_student(id))
        std::cout << "Removed"<<std::endl;
    std::cout<<std::endl;
}

void View_Control::Remove_course(Education_System &es)
{
    Show_msg("Enter course id: ");
    int id;
    std::cin >> id;
    if (es.Remove_course(id))
        std::cout << "Removed"<<std::endl;
    std::cout<<std::endl;
}

void View_Control::Remove_department(Education_System &es)
{    
    Show_msg("Enter department id: ");
    int id;
    std::cin >> id;
    if (es.Remove_department(id))
        std::cout << "Removed"<<std::endl;
    std::cout<<std::endl;
}
void View_Control::Remove_program(Education_System &es)
{    
    Show_msg("Enter program id: ");
    int id;
    std::cin >> id;
    if (es.Remove_program(id))
        std::cout << "Removed"<<std::endl;
    std::cout<<std::endl;
}
void View_Control::Assign_teacher_to_course(Education_System &es)
{
    Show_msg("Enter teacher id: ");
    int t_id;
    std::cin >> t_id;
    Show_msg("Enter course id: ");
    int c_id;
    std::cin >> c_id;
    if (es.Assign_teacher_to_course(t_id, c_id))
        std::cout << "Assigned"<<std::endl;
    std::cout<<std::endl;
}
void View_Control::Assign_student_to_course(Education_System &es)
{
    Show_msg("Enter student id: ");
    int s_id;
    std::cin >> s_id;
    Show_msg("Enter course id: ");
    int c_id;
    std::cin >> c_id;
    if (es.Assign_student_to_course(s_id, c_id))
        std::cout << "Assigned"<<std::endl;
    std::cout<<std::endl;
}

void View_Control::Assign_teacher_to_department(Education_System &es)
{
    Show_msg("Enter teacher id: ");
    int t_id;
    std::cin >> t_id;
    Show_msg("Enter department id: ");
    int d_id;
    std::cin >> d_id;
    if (es.Assign_teacher_to_department(t_id, d_id))
        std::cout << "Assigned"<<std::endl;
    std::cout<<std::endl;
}
void View_Control::Remove_teacher_from_course(Education_System &es)
{
    Show_msg("Enter teacher id: ");
    int t_id;
    std::cin >> t_id;
    Show_msg("Enter course id: ");
    int c_id;
    std::cin >> c_id;
    if (es.Remove_teacher_from_course(t_id, c_id))
        std::cout << "Removed"<<std::endl;
    std::cout<<std::endl;
}
void View_Control::Remove_student_from_course(Education_System &es)
{
    Show_msg("Enter student id: ");
    int s_id;
    std::cin >> s_id;
    Show_msg("Enter course id: ");
    int c_id;
    std::cin >> c_id;
    if (es.Remove_student_from_course(s_id, c_id))
        std::cout << "Removed"<<std::endl;
    std::cout<<std::endl;
}
void View_Control::Remove_teacher_from_department(Education_System &es)
{
    Show_msg("Enter teacher id: ");
    int t_id;
    std::cin >> t_id;
    Show_msg("Enter department id: ");
    int d_id;
    std::cin >> d_id;
    if (es.Remove_teacher_from_department(t_id, d_id))
        std::cout << "Removed"<<std::endl;
    std::cout<<std::endl;
}

void View_Control::Show_teacher_menu(Education_System &es)
{
    Show_delimeter();
    Show_endl("Teachers: ");
    Entity::Print_list(es.teachers);
    char c='\0';
    while (c!='A' && c!='a' && c!='R' && c!='r' && c!='I' && c!='i' && c!='C' && c!='c' && c!='D' && c!='d' && c!='X' && c!='x' && c!='Z' && c!='z' && c!='B' && c!='b')
    {
        std::cout<<std::endl;
        Show_endl("What do you want to do? Press the corresponding key");
        Show_endl("A - add teacher  ");
        Show_endl("R - remove teacher by id ");
        Show_endl("I - see info about teacher by id  ");
        Show_endl("C - assign teacher to a course by id ");
        Show_endl("D - assign teacher to a department by id ");
        Show_endl("X - remove teacher from a course by id ");
        Show_endl("Z - remove teacher from a department by id ");
        Show_endl("B - back  ");
        std::cin >> c;
    }
    switch (c)
        {
            case 'a':
            case 'A':
                Add_teacher(es);
                break;
            case 'R':
            case 'r':
                Remove_teacher(es);
                break;
            case 'I':
            case 'i':
                Display_teacher(es);
                break;
            case 'C':
            case 'c':
                Show_endl("Teachers: ");
                Entity::Print_list(es.teachers);
                Show_endl("Courses: ");
                Entity::Print_list(es.courses);
                Assign_teacher_to_course(es);
                break;
            case 'D':
            case 'd':
                Show_endl("Teachers: ");
                Entity::Print_list(es.teachers);
                Show_endl("Departments: ");
                Entity::Print_list(es.departments);
                Assign_teacher_to_department(es);
                break;
            case 'X':
            case 'x':
                Show_endl("Teachers: ");
                Entity::Print_list(es.teachers);
                Show_endl("Courses: ");
                Entity::Print_list(es.courses);
                Remove_teacher_from_course(es);
                break;
            case 'Z':
            case 'z':
                Show_endl("Teachers: ");
                Entity::Print_list(es.teachers);
                Show_endl("Departments: ");
                Entity::Print_list(es.departments);
                Remove_teacher_from_department(es);
                break;
            case 'B':
            case 'b':
                return;
        }
    Show_teacher_menu(es);    
}
void View_Control::Show_student_menu(Education_System &es)
{
    Show_delimeter();
    Show_endl("Students: ");
    Entity::Print_list(es.students);
    char c='\0';
    while (c!='A' && c!='a' && c!='R' && c!='r' && c!='I' && c!='i' && c!='C' && c!='c' && c!='X' && c!='x' && c!='B' && c!='b')
    {
        std::cout<<std::endl;
        Show_endl("What do you want to do? Press the corresponding key.");
        Show_endl("A - add student  ");
        Show_endl("R - remove student by id ");
        Show_endl("I - see info about student by id  ");
        Show_endl("C - assign student to a course by id ");
        Show_endl("X - remove student from a course by id ");
        Show_endl("B - back ");
        std::cin >> c;
    }
    switch (c)
        {
            case 'a':
            case 'A':
                Add_student(es);
                break;
            case 'R':
            case 'r':
                Remove_student(es);
                break;
            case 'I':
            case 'i':
                Display_student(es);
                break;
            case 'C':
            case 'c':
                Show_endl("Students: ");
                Entity::Print_list(es.students);
                Show_endl("Courses: ");
                Entity::Print_list(es.courses);
                Assign_student_to_course(es);
                break;
            case 'X':
            case 'x':
                Show_endl("Students: ");
                Entity::Print_list(es.students);
                Show_endl("Courses: ");
                Entity::Print_list(es.courses);
                Remove_student_from_course(es);
                break;
            case 'B':
            case 'b':
                return;
        }
    Show_student_menu(es);    
}
void View_Control::Show_course_menu(Education_System &es)
{
    Show_delimeter();
    Show_endl("Courses: ");
    Entity::Print_list(es.courses);
    char c='\0';
    while (c!='A' && c!='a' && c!='R' && c!='r' && c!='I' && c!='i' && c!='B' && c!='b')
    {
        std::cout<<std::endl;
        Show_endl("What do you want to do? Press the corresponding key");
        Show_endl("A - add course ");
        Show_endl("R - remove course by id ");
        Show_endl("I - see info about course by id  ");
        Show_endl("B - back  ");
        std::cin >> c;
    }
    switch (c)
        {
            case 'a':
            case 'A':
                Add_course(es);
                break;
            case 'R':
            case 'r':
                Remove_course(es);
                break;
            case 'I':
            case 'i':
                Display_course(es);
                break;
            case 'B':
            case 'b':
                return;
        }
    Show_course_menu(es);    
}

void View_Control::Show_department_menu(Education_System &es)
{
    Show_delimeter();
    Show_endl("Departments: ");
    Entity::Print_list(es.departments);
    char c='\0';
    while (c!='A' && c!='a' && c!='R' && c!='r' && c!='I' && c!='i' && c!='B' && c!='b')
    {
        std::cout<<std::endl;
        Show_endl("What do you want to do? Press the corresponding key");
        Show_endl("A - add department ");
        Show_endl("R - remove department by id ");
        Show_endl("I - see info about department by id  ");
        Show_endl("B - back  ");
        std::cin >> c;
    }
    switch (c)
        {
            case 'a':
            case 'A':
                Add_department(es);
                break;
            case 'R':
            case 'r':
                Remove_department(es);
                break;
            case 'I':
            case 'i':
                Display_department(es);
                break;
            case 'B':
            case 'b':
                return;
        }
    Show_department_menu(es);    
}
void View_Control::Show_program_menu(Education_System &es)
{
    Show_delimeter();
    Show_endl("Programs: ");
    Entity::Print_list(es.programs);
    char c='\0';
    while (c!='A' && c!='a' && c!='R' && c!='r' && c!='I' && c!='i' && c!='B' && c!='b')
    {
        std::cout<<std::endl;
        Show_endl("What do you want to do? Press the corresponding key");
        Show_endl("A - add program ");
        Show_endl("R - remove program by id ");
        Show_endl("I - see info about program by id  ");
        Show_endl("B - back  ");
        std::cin >> c;
    }
    switch (c)
        {
            case 'a':
            case 'A':
                Add_program(es);
                break;
            case 'R':
            case 'r':
                Remove_program(es);
                break;
            case 'I':
            case 'i':
                Display_program(es);
                break;
            case 'B':
            case 'b':
                return;
        }
    Show_program_menu(es);    
}

void View_Control::Show_main_menu(Education_System &es)
{
    Show_delimeter();
    es.Print_system();
    char c='\0';
    while (c!='T' && c!='t' && c!='S' && c!='s' && c!='C' && c!='c' && c!='D' && c!='d' && c!='P' && c!='p' && c!='Q' && c!='q')
    {
        std::cout<<std::endl;
        Show_endl("What do you want to do? Press the corresponding key.");
        Show_endl("T - Teachers: add / remove / see / assign to or remove from course or department");
        Show_endl("S - Students: add / remove / see / assign to or remove from course");
        Show_endl("C - Courses: add / remove / see");
        Show_endl("D - Departments: add / remove / see");
        Show_endl("P - Programs: add / remove / see");
        Show_endl("Q - exit  ");
        std::cin >> c;
    }
    switch (c)
        {
            case 'T':
            case 't':
                Show_teacher_menu(es);
                break;
            case 'S':
            case 's':
                Show_student_menu(es);
                break;
            case 'C':
            case 'c':
                Show_course_menu(es);                
                break;
            case 'D':
            case 'd':
                Show_department_menu(es);                
                break;
            case 'P':
            case 'p':
                Show_program_menu(es);                
                break;
            case 'Q':
            case 'q':
                return;
        }
    Show_main_menu(es);
}
