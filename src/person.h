#ifndef PERSON_H
#define PERSON_H
#include "entity.h"
#include <iostream>
#include <string>

class Person
{
    protected:
        std::string f_name;
        std::string s_name;
        Person(){};
        Person (std::string f_name, std::string s_name)
        {
            this->f_name=f_name;
            this->s_name=s_name;
        }         
};
#endif