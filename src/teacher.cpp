#include <string>
#include <vector>
#include "person.h"
#include <iostream>
#include "teacher.h"

Teacher::Teacher(std::string f_name, std::string s_name, float salary, int id) : Person (f_name, s_name) 
{
    this->id = id;
    this->salary = salary;
}
void Teacher::Print()
{
    std::cout << this->id << ": ";
    std::cout << f_name << ' ' << s_name << ", ";
    std::cout << "salary: " << salary << std::endl;
}