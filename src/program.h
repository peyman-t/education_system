#ifndef PROGRAM_H
#define PROGRAM_H
#include <string>
#include "entity.h"
class Program : public Entity
{
    private:
        int id; 
        std::string name;
        float acceptance_rate;
    public:
        Program(){};
        Program (std::string name, float acceptance_rate, int id);
        void Print();
        int Get_id() { return id; }
        template <class B>
        void serialize(B& buf) const 
        {
            buf << id  << name << acceptance_rate;
        }
        template <class B>
        void parse(B& buf) 
        {
            buf >> id >> name >> acceptance_rate;
        }
};
#endif
