#ifndef CONTROL_H
#define CONTROL_H
#include "education_system.h"
#include <string>
/*
*@brief the class to operate with the input and ouput
*/
class View_Control
{
        void Show_delimeter();
        void Show_msg(std::string s);
        void Show_endl(std::string s);
    public:     
        void Display_teacher(Education_System &es);
        void Display_student(Education_System &es);
        void Display_course(Education_System &es);
        void Display_department(Education_System &es);
        void Display_program(Education_System &es);
        
        void Add_teacher(Education_System &es);
        void Add_student(Education_System &es);
        void Add_course(Education_System &es);
        void Add_department(Education_System &es);
        void Add_program(Education_System &es);       

        void Remove_teacher(Education_System &es);
        void Remove_student(Education_System &es);
        void Remove_course(Education_System &es);
        void Remove_department(Education_System &es);
        void Remove_program(Education_System &es);

        void Assign_teacher_to_course(Education_System &es);
        void Assign_student_to_course(Education_System &es);
        void Assign_teacher_to_department(Education_System &es);

        void Remove_teacher_from_course(Education_System &es);
        void Remove_student_from_course(Education_System &es);
        void Remove_teacher_from_department(Education_System &es);
              
        void Show_teacher_menu(Education_System &es);
        void Show_student_menu(Education_System &es);
        void Show_course_menu(Education_System &es);
        void Show_department_menu(Education_System &es);
        void Show_program_menu(Education_System &es);
        void Show_main_menu(Education_System &es);
};
#endif